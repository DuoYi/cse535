<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Algorithm Visualization</title>
	<style type="text/css">
		a{
			text-decoration: underline;
			color:blue;
			cursor:pointer;
		}
        input#chat {
            width: 100%
        }
		.chatdiv{
			width: 300px;
		}
		.inst {
		    display: block;
		    -webkit-transition: height 1s, opacity 1s ease-in-out;
		    transition: height 1s,	opacity 1s ease-out;
		    opacity: 0; 
		    height: 0;
		    width:80%;
		    overflow: hidden;
		}
		
		.inst.active {
		    opacity: 1;
		    height: 100%;
		    -webkit-transition: height 1s, opacity 1s ease-in;
		    transition: height 1s,	opacity 1s ease-in;
		}
        #console-container {
            width: 400px;
            resize: both;
        }
		.corner{
			position: fixed;
			right: 0;
			bottom: 0;
			border: 1px solid black;
			padding: 10px;
			box-shadow: 0 0 10px black;
			max-width: 300px;
			background:white;
			text-align:right;
		}
        .console {
            border: 1px solid #CCCCCC;
            border-right-color: #999999;
            border-bottom-color: #999999;
            height: 170px;
            overflow-y: scroll;
            padding: 5px;
            width: 99%;
            text-align:left;
        }

        .console p {
            padding: 0;
            margin: 0;
            word-wrap: break-word;
        }
        #procs{
        	list-style:none;
        	padding:0;
        	margin:1em auto;
        }
        #procs li{
        	display: inline-table;
			text-align: center;
			padding: 30px;
			border: 1px solid silver;
        }
        #procs:empty{
        	display:none;
        }
        .warning{
        	color: #880;
        }
        .hide{
        	display:none;
        }
        .idle{
        	background-color: #EEE;
        }
        .working{
        	background-color: #7E7;
        }
        .requesting{
        	background-color: #FF4;
        }
        .unknown{
        	background-color: #FCC;
        }
    </style>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>
            if (!window.jQuery) { //fallback
                document.write('<script src="/cse535/Scripts/jquery-1.10.2.min.js"><\/script>');
            }
        </script>
	<%@ page import="models.RunningState, org.json.simple.*" %>
	<%
		ServletContext context = pageContext.getServletContext();
		RunningState state = (RunningState)context.getAttribute("RunningState");
		
		
	%>
    <script type="application/javascript">
        var Chat = {};
        var processes = <%= JSONValue.toJSONString(state.instanceStates) %>;
        var historylog = <%= JSONValue.toJSONString(state.historyLog) %>;
        var keys = [];
		var keyid = {};
		var count =0;
        Chat.socket = null;

        Chat.connect = (function(host) {
            if ('WebSocket' in window) {
                Chat.socket = new WebSocket(host);
            } else if ('MozWebSocket' in window) {
                Chat.socket = new MozWebSocket(host);
            } else {
                chatmsg('Error: WebSocket is not supported by this browser.');
                return;
            }

            Chat.socket.onopen = function () {
                chatmsg('Info: WebSocket connection opened.');
                document.getElementById('chat').onkeydown = function(event) {
                    if (event.keyCode == 13) {
                        Chat.sendMessage();
                    }
                };
            };

            Chat.socket.onclose = function () {
                document.getElementById('chat').onkeydown = null;
                chatmsg('Info: WebSocket closed.');
            };
			//tokens:0=original msg, 1=process name, 2=operation, 3=warning 
            Chat.socket.onmessage = function (message) {
				if(message.data[0] === '>'){ //user msg
					chatmsg(message.data.slice(1));
					return;
				}
				else if(message.data[0] === '<'){
					chatmsg(message.data.slice(1));
					doreset();
					return;
				}
            	var tokens = message.data.split('|');
            	for(var i=0;i<tokens.length;i++){
            		tokens[i] = tokens[i].trim();
            	}
                logmsg(tokens[0]);
                if(tokens[3] && tokens[3] != "null" && tokens[3].length > 0 && tokens[3].trim()){
                	logmsg("warning: " + tokens[3],true);
                }
                if(tokens.length > 2){
                	if(!processes[tokens[1]]){
                		var i=0;
                		for(;i<keys.length;i++){
                			if(keys[i] > tokens[1])
                				break;
                		}
                		keys.splice(i,0,tokens[1]);
                		keyid[tokens[1]] = count++;
                		if(keys.length == 1 || keys.length == i+1)
                			$('#procs').append(listitem(tokens[2],tokens[1],count-1));
                		else
                			$('#procs>li').eq(i).before(listitem(tokens[2],tokens[1],count-1));
                	}
                	processes[tokens[1]] = tokens[2];
                	$('#i'+keyid[tokens[1]]).attr("class", tokens[2]);
                }
            };
        });

        Chat.initialize = function() {
            if (window.location.protocol == 'http:') {
                Chat.connect('ws://' + window.location.host + '/cse535/chat');
            } else {
                Chat.connect('wss://' + window.location.host + '/cse535/chat');
            }
        };

        Chat.sendMessage = (function() {
            var message = document.getElementById('chat').value;
            if (message != '') {
                Chat.socket.send(message);
                document.getElementById('chat').value = '';
            }
        });

        //var Console = {};
		
        function listitem(classname, content, id){
        	if(id === undefined)
        		id=content;
        	return "<li class='"+classname+"' id='i"+id+"'>"+content+"</li>";
        }
        
        
        function logmsg(message, iswarning) {
            var console = document.getElementById('console');
            var p = document.createElement('p');
            if(iswarning){
            	p.className = 'warning';
            	if(document.getElementById("showwarn").checked)
            		p.className += " hide";
            }
            p.innerHTML = message;
            console.appendChild(p);
            while (console.childNodes.length > 100) {
                console.removeChild(console.firstChild);
            }
            console.scrollTop = console.scrollHeight;
        };
        function chatmsg(message) {
            var console = document.getElementById('chatconsole');
            var p = document.createElement('p');
            p.innerHTML = message;
            console.appendChild(p);
            while (console.childNodes.length > 100) {
                console.removeChild(console.firstChild);
            }
            console.scrollTop = console.scrollHeight;
        };

        Chat.initialize();


        document.addEventListener("DOMContentLoaded", function() {
           
            var ulist = $('#procs');
            for (var p in processes){
            	keys.push(p);
            	keyid[p]=count++;
            }
            keys.sort();
            for (var i=0; i<keys.length; i++){
            	ulist.append(listitem(processes[keys[i]],keys[i],keyid[keys[i]]));
            }
            for (var i=0; i<historylog.length; i++){
            	logmsg(historylog[i], (historylog[i].indexOf("warning") == 0));
            }
            
            $('#showwarn').click(function(){
            	$(".warning").toggleClass('hide');
            })
        }, false);
		var error;
        function reset(){
        	if(confirm("You sure?")){
        		$.post("/cse535/reset", true).fail(function(e){
        			error = e;
        			alert("reset failed");
        		});
        	}
        }
        function doreset(){
   			keys = [];
   			keyid = {};
   			count =0;
   			processes = {};
           	historylog = [];
           	$('#console').empty();
           	$('#procs').empty();
        }
        function togglechat(){
        	$(".chatdiv").toggle(1000);
        }
        function toggleInst(){
        	$('.inst').toggleClass('active');
        }
    </script>
</head>

<body>
<noscript><h2 style="color: #ff0000">Seems your browser doesn't support Javascript! Websockets rely on Javascript being enabled. Please enable
    Javascript and reload this page!</h2>
</noscript>
<div>
	<ul id="procs">
	</ul>
    <div id="console-container">
        <div class="console" id="console"></div>
    </div>
    <label for="showwarn">
    	<input type="checkbox" id="showwarn">
    	Hide warnings
    </label>
    <button onclick="reset();">Reset</button>

   	<p>Legend:<strong class="unknown">unknown</strong> <strong class="idle">idle</strong> 
   	<strong class="requesting">requesting</strong> <strong class="working">working</strong> </p>
   	<h3><a onclick="toggleInst()">Instructions</a></h3>
 	<div class="inst">
    	<hr>
    	<p>To send algorithm data to the server, send <strong>UDP</strong> packet to 54.200.55.63:59144.</p>
    	<p>All data should be send as string. The currect format for strings is: 
    	<strong>[process ID] [verb] [additional words...]</strong>
    	</p>
    	<p>Expected verb type:</p>
   		<ul>
   		<li>Initailization (optional but will produce warning if omitted):<strong>start</strong></li>
   		<li>Requesting: <strong>request/requesting</strong></li>
   		<li>Enter CS: <strong>in</strong> <em>or</em> <strong>enter</strong></li>
   		<li>Leaving: <strong>exit</strong> <em>or</em> <strong>release/releasing</strong></li>
   		</ul>
    	<p>If a verb not listed above is received, the status of the process will become UNKOWN.</p>
    	<p>Message without any space seperating will be displayed without parsing. 
    	Surround ID with bracet () if there are space in it.</p>
    	<p>Data start with &gt; will be diplayed in the chat box.</p>
    </div>
    <div class="corner">
    <button onclick="togglechat()">hide/show</button><br>
    <div class="chatdiv">
	    <p>
	        <input type="text" placeholder="type and press enter to chat" id="chat" />
	    </p>
	    <div class="console" id="chatconsole"></div>
    </div>
    </div>
</div>
</body>
</html>
