package models;

public final class InstanceState {
	public static final String unknown = "unknown" ;
	public static final String idle = "idle";
	public static final String requesting = "requesting";
	public static final String working = "working";
	
}

