package models;
import java.util.*;
import java.util.logging.Logger;

import util.HTMLFilter;

public class RunningState {
	public int instanceCount;
	public List<String> historyLog;
	public Map<String, String> instanceStates;
	private static Logger log = Logger.getLogger(RunningState.class.getName());
	
	public RunningState(){
		this.historyLog = new ArrayList<String>();
		this.instanceStates = new HashMap<String, String>();
	}
	
	public void empty(){
		instanceCount = 0;
		historyLog.clear();
		instanceStates.clear();
		log.info("running state reset");
	}
	
	public String NewMsg(String msg){
		if(msg.startsWith(">")){
			historyLog.add(msg);
			return msg;
		}
		msg = msg.trim().replaceAll("\\|", "_");
		msg = HTMLFilter.filter(msg);
		String[] words;
		int pos = msg.indexOf(")")+1;
		if(pos >= 0){
			List<String> tmp = new ArrayList<String>();
			tmp.add(msg.substring(0,pos));
			tmp.addAll(Arrays.asList(msg.substring(pos).trim().split("\\s+")));
			words = new String[tmp.size()];
			tmp.toArray(words);
		}
		else
			words = msg.split("\\s+");
/*		StringBuilder sb = new StringBuilder();
		for(String s: words){
			sb.append(s);
			sb.append(", ");
		}
*/		log.info(words[1]);
		historyLog.add(msg);
		if(words.length < 2)
			return msg;
		String name = words[0];
		String warning = null;
		String op = null;
		String state = instanceStates.get(name);
		String verb = words[1].toLowerCase();
		if(state == null){
			instanceStates.put(name, InstanceState.unknown);
		}
		if(verb.indexOf("start") >= 0){
			if(state != null){
				warning = "process already started";
			}
			op = "idle";
			instanceStates.put(name, InstanceState.idle);
		}
		else if(verb.indexOf("request") >= 0){
			if(state != InstanceState.idle && state != InstanceState.requesting && state != InstanceState.unknown){
				warning = "non-idle process request";
				log.info(warning + " STATE= " + state);
			}
			op = "requesting";
			instanceStates.put(name, InstanceState.requesting);
		}
		else if(verb.indexOf("enter") >= 0 || verb.equalsIgnoreCase("in")){
			if(state != InstanceState.requesting){
				warning = "process entered without request";
			}
			op = "working";
			instanceStates.put(name, InstanceState.working);
		}
		else if(verb.indexOf("exit") >= 0 || verb.indexOf("releas") >= 0){
			if(state != InstanceState.working){
				warning = "process exited without enter";
			}
			op = "idle";
			instanceStates.put(name, InstanceState.idle);
		}
		else
			op = "unknown";
		if(warning != null)
			historyLog.add("warning: "+warning);
		return String.format("%s|%s|%s|%s", msg, words[0], op, warning);
	}
}
