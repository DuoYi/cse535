package controllers;

import javax.servlet.*;

public class UDPListener implements ServletContextListener {
	private UDPTask task = null;
	private Thread thread = null;
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		if(thread != null){
			task.terminate();
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		task = new UDPTask();
		arg0.getServletContext().setAttribute("RunningState", task.state);
		thread = new Thread(task);
		thread.start();
	}

}
