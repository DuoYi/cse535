package controllers;
import java.net.*;
import java.util.logging.*;
import models.RunningState;;

public class UDPTask implements Runnable {
	private volatile boolean running = true;
	private DatagramSocket socket;
	private byte[] receiveData = new byte[1024];
	private static Logger log = Logger.getLogger(UDPTask.class.getName());
	public RunningState state = new RunningState();
    public void terminate() {
        running = false;
    }
	@Override
	public void run() {
		log.info("-----task started-----");
		try {
			socket = new DatagramSocket(59144);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			terminate();
		}
		while(running){
			log.info("waiting for packet");
			DatagramPacket p = new DatagramPacket(receiveData, receiveData.length);
			try {
				socket.receive(p);
				ChatController.broadcast(state.NewMsg(new String(p.getData(),0, p.getLength())));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				terminate();
			}
		}
	}

}
