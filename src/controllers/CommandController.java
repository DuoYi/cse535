package controllers;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import models.RunningState;

public class CommandController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest request,
	            HttpServletResponse response)
		throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println("Hello World");
	}
	public void doPost(HttpServletRequest request,
            HttpServletResponse response)
	throws ServletException, IOException {
		ServletContext context = getServletContext();
		RunningState state = (RunningState)context.getAttribute("RunningState");
		state.empty();
		ChatController.broadcast("<Running state reset");
	PrintWriter out = response.getWriter();
	out.println("done");
}
}
